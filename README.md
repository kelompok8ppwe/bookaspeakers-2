## BookaSpeaker

**KE08 - PPW E**
**Fakultas Ilmu Komputer**

## Anggota Kelompok
Carlo Tupa Indriauan - 1806186862

Hanifah Rahmajati - 1806146972

Jonathan - 1806204985

Nethania Sonya Violencia Lasmaria - 1806205514

## Link BookaSpeaker
[![pipeline status](https://gitlab.com/kelompok8ppwe/bookaspeakers-2/badges/master/pipeline.svg)](https://gitlab.com/kelompok8ppwe/bookaspeakers-2/commits/master)


## Link Heroku 
http://bookaspeaker-2.herokuapp.com/


## Deskripsi BookaSpeaker
Web yang kami buat adalah BookaSpeaker. Dimana BookaSpeaker membantu mempertemukan antara klien dengan pembicara, sehingga web ini membantu pemilik acara untuk mencari pembicara. Tak hanya itu BookaSpeaker memberikan kesempatan bagi seseorang yang ingin mendaftarkan dirinya sebagai pembicara, sehingga dapat membantu memberikan exposure kepada speaker karena semakin sering seorang speaker dibook oleh klien, maka speaker tersebut berpotensi menjadi salah satu top speaker yang akan ditampilkan pada Hompage BookaSpeaker. Fitur lain yang ditawarkan oleh BookaSpeaker adalah mengelompokkan pembicara sesuai kategorinya sehingga klien akan lebih mudah dalam mencari berdasarkan tema yang ingin dibawa dalam acaranya. Selain mengelompokkan sesuai kategori, BookaSpeaker juga memberikan fitur untuk mencari berdasarkan kota atapun nama, dengan ini klien bisa mencari pembicara terdekat. BookaSpeaker juga memberikan kemudahan klien yang membutuhkan pembicara dengan budget tertentu, karena BookaSpeaker memberikan fitur melihat list pembicara berdasarkan harga. 
