from django.shortcuts import render
from django.core import serializers
from django.http import HttpResponse
from django.middleware.csrf import get_token
from registerPembicara import forms, models

def index(request):
    form = forms.RegisterForm()
    pembicaras = models.Pembicara.objects.all().order_by('-counter')[0:3]
    pemb = {
        'pembicara_data' : pembicaras,
    }
    return render(request, "index.html", pemb)

def getCounter(request):
    pembicaras = models.Pembicara.objects.all().order_by('-counter')[0:3]
    pembicara_list = serializers.serialize('json', pembicaras)
    return HttpResponse(pembicara_list, content_type="text/json-comment-filtered")

