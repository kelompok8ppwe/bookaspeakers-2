from django.test import TestCase, Client
from registerPembicara import forms, models
from registerPembicara.models import Pembicara


class homepageTest(TestCase):
    def test_URL(self):
        c = Client()
        response = c.get('')
        self.assertEqual(response.status_code, 200)
    
    def test_namaweb(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn("BookaSpeaker", content)
    
    def test_topspeaker(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn("Top Speakers", content)
    
    def test_button(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content)
        self.assertIn("Join as Speaker", content)
    
    # def test3topspeaker(self):
    #     c = Client()
    #     a = Pembicara.objects.create(first_name="eva", last_name="evaa", position="Manager", email="test@yahoo.com", gender="laki", city="jakarta", fee="20000", birth_date="2000-06-02", phone="09372873862", desc="halo")
    #     counter1 = Pembicara.objects.count()
    #     pembicaras = models.Pembicara.objects.all().order_by('-counter')[0:3]
    #     response = c.get('')
    #     content = response.content.decode('utf8') 
    #     self.assertIn(pembicaras[0].first_name, content)



# Create your tests here.
