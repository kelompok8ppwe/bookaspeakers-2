from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf import settings

app_name="search"

urlpatterns = [
    path('', views.search, name="search"),
    path('getData/', views.getData, name="get-data"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)