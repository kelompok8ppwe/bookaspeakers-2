from django.test import TestCase, Client
from django.urls import resolve
from . import views
from searchapp.views import search, getData
from registerPembicara.models import Pembicara

c = Client() 
response = c.get('/search/')

class searchappTest(TestCase):
    def test_apakah_benar_ada_url_slash_search(self):
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_benar_ada_url_slash_search_slash_getData(self):
        response = c.get('/search/getData/')
        self.assertEqual(response.status_code, 200)

    def test_fungsi_getData(self):
        response = getData(c)
        self.assertEqual(response._content_type_for_repr[3:-1], 'text/json-comment-filtered')

    def test_apakah_menggunakan_search_html(self):
        self.assertTemplateUsed(response, 'search.html')

    def test_apakah_menggunakan_fungsi_search(self):
        response = resolve('/search/')
        self.assertEqual(response.func, views.search)

    def test_apakah_menggunakan_fungsi_getData(self):
        response = resolve('/search/getData/')
        self.assertEqual(response.func, views.getData)

    def test_apakah_ada_tulisan_speakers(self):
        content = response.content.decode('utf8') 
        self.assertIn ('Speakers', content)