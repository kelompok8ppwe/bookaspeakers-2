const myUrl = "http://bookaspeaker-2.herokuapp.com"

const formatPembicara = (pembicara) => {
    $("#sp-result").append(
        `<div class="card-pembicara">
            <div class="foto-pembicara">
                <img src="/media/${pembicara.fields.photo}" class="img-fluid imgtop" alt="image">
            </div>
            <div class="primary">
                ${pembicara.fields.first_name} </br> ${pembicara.fields.last_name}
            </div>
            <div class="primary jabatan-pembicara">
                ${pembicara.fields.position}
            </div>
            <div class="container-kategori">
                <div class="primary kategori-pembicara">
                    ${pembicara.fields.category}
                </div>
            </div>
            <div class="primary ket-pembicara">
                <i class="fas fa-map-marker-alt"></i> ${pembicara.fields.city} </br>
                <i class="fas fa-tags"></i> Rp ${pembicara.fields.fee}
            </div>
            <div class="primary">
                <a href="/profile/${pembicara.pk}">See More</a>
            </div>
        </div>`
    );
}

const formatSemuaPembicara = (data) => {  
    data.forEach(function(pembicara, index){
        formatPembicara(pembicara)
    })
}
const formatSearchRecommendation = (data, key) => {
    data.forEach(function(pembicara, index){
        if(pembicara.fields.first_name.toLowerCase().includes(key.toLowerCase()) || pembicara.fields.last_name.toLowerCase().includes(key.toLowerCase())){
            formatPembicara(pembicara);
        }  
    })
}

$(document).ready(() => {
    $.ajax({
        url: `/search/getData/`,
        success: function(res){
            $("#sp-result").empty();
            formatSemuaPembicara(res)
        }
    })

    $("#search_input").on("keyup", function(e){
        var key = e.currentTarget.value;
        console.log(key)
        if(key != ""){
            $.ajax({
                url: `/search/getData/`,
                success: function(res){
                    $("#sp-result").empty();
                    formatSearchRecommendation(res, key)
                },
            })
        }else{
            $.ajax({
                url: `/search/getData/`,
                success: function(res){
                    $("#sp-result").empty();
                    formatSemuaPembicara(res, key)
                }
            })
        }
    })
})