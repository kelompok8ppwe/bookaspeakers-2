from django.shortcuts import render
from django.core import serializers
from django.http import HttpResponse

from registerPembicara.models import Pembicara

# Create your views here.
def search(request):
    data = Pembicara.objects.all()
    return render(request, 'search.html', {'data_pembicara':data})

def getData(request):
    pembicara = Pembicara.objects.all()
    pembicara_list = serializers.serialize('json', pembicara)
    return HttpResponse(pembicara_list, content_type="text/json-comment-filtered")