from django.test import TestCase, Client
import unittest
from django.contrib.auth.models import User

class Unittest(TestCase) :
    def test_url_login(self):
        response = Client().get('/account/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_url_register(self):
        response = Client().get('/account/register/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registeracc.html')

    def test_button_dilogin(self):
        c = Client()
        response = c.get('/account/login/')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content) 
        self.assertIn("Login", content)
    
    def test_bisa_login(self):
        user = User.objects.create_user('Eva', 'Evaa@gmail.com', 'cobain123')
        response = Client().post('/account/login/', {'username': 'Eva', 'password': 'cobain123'})
        self.assertEqual(response.status_code, 302)
    
    def test_gagal_login(self):
        user = User.objects.create_user('Eva', 'Evaa@gmail.com', 'cobain123')
        response = Client().post('account/login/', {'username': 'Eva', 'password': 'cobain321'})
        

