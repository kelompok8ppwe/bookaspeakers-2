from django import forms
from . import models

class DateInput(forms.DateInput):
    input_type = "date"
    
class RegisterForm(forms.ModelForm):
    class Meta:
        model = models.Pembicara
        fields = ['first_name', 'last_name', 'position', 'email',
                'gender', 'category', 'city', 'fee', 'birth_date',
                'phone', 'desc', 'photo'
                ]

        widgets = {
            'birth_date': DateInput
        }
        
        