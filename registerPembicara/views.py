from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import forms
from . import models
from django.core import serializers

# Create your views here.
# def register(request):
#     form = forms.RegisterForm()
#     return render(request, 'register.html', {'form':form})

def register(request):
    if request.method == "POST":
        form = forms.RegisterForm(request.POST, request.FILES)
        print(form.is_valid)  
        if form.is_valid(): 
            form.save()
    else:
        form = forms.RegisterForm()
    return render(request, 'register.html', {'form':form})

def confirm(request):
    return render(request, 'confirm.html')

def getData(request):
    pembicaras = models.Pembicara.objects.all().order_by("?")
    pembicara_list = serializers.serialize('json', pembicaras)
    return HttpResponse(pembicara_list, content_type="text/json-comment-filtered")