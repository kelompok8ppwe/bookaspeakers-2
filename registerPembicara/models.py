from django.db import models
from django.core.validators import RegexValidator

class Pembicara(models.Model):
    GENDER = [
        ('male', "Male"),
        ('female', "Female"),
    ]

    CATEGORY = [
        ('IT', "IT"),
        ('Entertainment', "Entertainment"),
        ('Beauty', "Beauty"),
        ('Business', "Business")
    ]

    numeric = RegexValidator(r'^[0-9]*$')

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    position = models.CharField(max_length=100)
    email = models.EmailField(max_length=254)
    gender = models.CharField(max_length=6, choices=GENDER, default='male')
    category = models.CharField(max_length=100, choices=CATEGORY, default='IT')
    city = models.CharField(max_length=100)
    fee = models.BigIntegerField()
    birth_date = models.DateField(auto_now=False, auto_now_add=False)
    phone = models.CharField(max_length= 15, validators=[numeric])
    desc = models.TextField(max_length=300)
    photo = models.ImageField(upload_to='pic/', blank=True)
    counter = models.BigIntegerField(default=0)

    def __str__(self):
        return self.first_name + " " + self.last_name

