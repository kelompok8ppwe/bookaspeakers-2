$(document).ready(function() {
    $('.create_button').mouseenter(function() {
        $('.kotak_kiri').css("opacity", "1")
        $('.kotak_kanan').css("opacity", "1")
        $('.kotak_kiri').css("transform", "scaleX(200)")
        $('.kotak_kanan').css("transform", "scaleX(200)")
    })

    $('.create_button').mouseleave(function() {
        $('.kotak_kiri').css("opacity", "0")
        $('.kotak_kanan').css("opacity", "0")
        $('.kotak_kiri').css("transform", "scaleX(0)")
        $('.kotak_kanan').css("transform", "scaleX(0)")
    })

    $('#id_desc').on("input", function(e) {
        var panjang = e.target.value.length
        $('#jumlah_input').html(panjang)
    })

    $("input").focus(function(e) {
        var focused = $(this);
        $('.form_container > label').each(function(f) {
            var ini = $(this);
            if(ini.hasClass('focused')) {
                ini.removeClass('focused');
            }
        })
       focused.parent().find('label').addClass('focused');
    })

    $("textarea").focus(function(e) {
        var focused = $(this);
        $('.form_container > label').each(function(f) {
            var ini = $(this);
            if(ini.hasClass('focused')) {
                ini.removeClass('focused');
            }
        })
       focused.parent().find('label').addClass('focused');
    })
    $("select").focus(function(e) {
        var focused = $(this);
        $('.form_container > label').each(function(f) {
            var ini = $(this);
            if(ini.hasClass('focused')) {
                ini.removeClass('focused');
            }
        })
       focused.parent().find('label').addClass('focused');
    })

    $('.create_button').click(function() {
        const csrf = $('input[name="csrfmiddlewaretoken"]');
        let firstname = $('#id_first_name').val();
        let lastname = $('#id_last_name').val();
        let position = $('#id_position').val();
        let email = $('#id_email').val();
        let gender = $('#id_gender').val();
        let category = $('#id_category').val();
        let city = $('#id_city').val();
        let fee = $('#id_fee').val();
        let birth = $('#id_birth_date').val();
        let phone = $('#id_phone').val();
        let desc = $('#id_desc').val();
        let photo = $('#id_photo').val();   
        let counter = 0;
        if(firstname != "" &&
            lastname != "" &&
            position != "" &&
            email != "" &&
            city != "" &&
            fee != "" &&
            birth != "" &&
            phone != "" &&
            desc != "") {
                $.ajax( {
                    method: 'POST',
                    url: "/register/",
                    data: {
                        csrfmiddlewaretoken : csrf[0].value,
                        first_name : firstname,
                        last_name : lastname,
                        position : position,
                        email : email,
                        gender : gender,
                        category : category,
                        city : city,
                        fee : fee,
                        birth_date : birth,
                        phone : phone,
                        desc : desc,
                        photo : photo,
                        counter : counter
                    },
                    success: function(response) {
                        $('#id_first_name').val('');
                        $('#id_last_name').val('');
                        $('#id_position').val('');
                        $('#id_email').val('');
                        $('#id_city').val('');
                        $('#id_fee').val('');
                        $('#id_birth_date').val('');
                        $('#id_phone').val('');
                        $('#id_desc').val('');
                        alert("Registrasi Berhasil!")
                    }
                })
            }
    })

    $('.random_button').click(function(){
        $.ajax({
            method: 'GET',
            url: "/register/getData",
            success: function(response) {
                console.log(response);
                $('.daftar').empty();
                $('.daftar').append('<p>' + response[0].fields.first_name + ' ' + response[0].fields.last_name + "</p>");
                $('.daftar').append('<p>' + response[1].fields.first_name + ' ' + response[1].fields.last_name + "</p>");
                $('.daftar').append('<p>' + response[2].fields.first_name + ' ' + response[2].fields.last_name + "</p>");
            }
        }) 
    })  
})