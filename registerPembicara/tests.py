from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .models import Pembicara

c = Client() 
response = c.get('/register/')

class registerPembicaraTest(TestCase):
    def test_apakah_benar_ada_url_slash_register(self):
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_menggunakan_register_html(self):
        self.assertTemplateUsed(response, 'register.html')

    def test_apakah_menggunakan_fungsi_register(self):
        response = resolve('/register/')
        self.assertEqual(response.func, views.register)

    def test_apakah_nama_models_adalah_pembicara(self):
        Pembicara(
            first_name= 'jonathan',
            last_name= 'Chandra',
            position = 'CEO',
            email= 'Jonathan@yahoo.com',
            gender= 'male',
            category= 'IT',
            city= 'Depok',
            fee= '200000',
            birth_date= '2000-06-05',    
            phone= '087887537138',
            desc= 'Ini test').save()
        pembicara = Pembicara.objects.all().get(first_name = 'jonathan')
        nama = pembicara.first_name + " " + pembicara.last_name
        self.assertEqual(str(pembicara), nama)

    def test_apakah_ada_tulisan_register_as_speaker(self):
        content = response.content.decode('utf8') 
        self.assertIn ('You must login first to register as speaker', content)
    
    def test_apakah_form_register_bekerja_atau_tidak(self):
        response = c.post('/register/', data = {
            'first_name': 'jonathan',
            'last_name': 'Chandra',
            'position' : 'CEO',
            'email': 'Jonathan@yahoo.com',
            'gender': 'male',
            'category': 'IT',
            'city': 'Depok',
            'fee': '200000',
            'birth_date': '02/06/2000',    
            'phone': '087887537138',
            'desc': 'Ini test'})
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_speaker_bisa_tersimpan_atau_tidak(self):
        Pembicara(
            first_name= 'jonathan',
            last_name= 'Chandra',
            position = 'CEO',
            email= 'Jonathan@yahoo.com',
            gender= 'male',
            category= 'IT',
            city= 'Depok',
            fee= '200000',
            birth_date= '2000-06-05',    
            phone= '087887537138',
            desc= 'Ini test').save()
        self.assertEqual(Pembicara.objects.all().count(), 1)

    def test_apakah_benar_ada_url_slash_register_slash_confirm(self):
        response = c.get('/register/confirm')
        self.assertEqual(response.status_code, 200)

    def test_apakah_menggunakan_confirm_html(self):
        response = c.get('/register/confirm')
        self.assertTemplateUsed(response, 'confirm.html')

    def test_apakah_menggunakan_fungsi_confirm(self):
        response = resolve('/register/confirm')
        self.assertEqual(response.func, views.confirm)
    
    def test_apakah_ada_tulisan_Register_Succeed(self):
        response = c.get('/register/confirm')
        content = response.content.decode('utf8') 
        self.assertIn("Register Succeed", content)

    def test_apakah_ada_tulisan_Back_dan_Home(self):
        response = c.get('/register/confirm')
        content = response.content.decode('utf8') 
        self.assertIn("Back", content)
        self.assertIn("Home", content)
    


