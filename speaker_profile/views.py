from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .forms import CreateBooking
from .models import Booking
from registerPembicara.models import Pembicara
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import F
from django.contrib.auth.decorators import login_required

# Create your views here.
def profile(request, id):
    form = CreateBooking(request.POST or None)
    speaker = Pembicara.objects.get(id = id)
    if (request.method == 'POST' and form.is_valid()):
        map = {
            'name': request.POST['name'],
            'event_name': request.POST['event_name'],
            'event_date': request.POST['event_date'],
            'topic': request.POST['topic'],
            'city': request.POST['city'],
            'phone_number': request.POST['phone_number'],
            'email': request.POST['email'],
            'form': CreateBooking(),
            'data': speaker,
        }
        new_entry = Booking(name=map['name'], event_name=map['event_name'], event_date=map['event_date'], topic=map['topic'], city=map['city'], phone_number=map['phone_number'], email=map['email'], speaker=speaker)
        new_entry.save()
        speaker.counter = F('counter') + 1
        speaker.save()
        return render(request, 'profile.html', map)
    else:
        map = {
            'form': CreateBooking(),
            'data': speaker,
        }
        return render(request, 'profile.html', map)

def check_availability(request, id):
    if request.method == "GET":
        response_data = {}
        event_date = request.GET.get('event_date', False)
        check = None
        try:
            try:
                speaker = Pembicara.objects.get(id = id)
                filtered = Booking.objects.filter(speaker = speaker)
                check = filtered.get(event_date = event_date)
            except ObjectDoesNotExist as e:
                pass
            except Exception as e:
                raise e
            if not check:
                response_data['available'] = 'ok'
            else:
                response_data['available'] = 'no'
        except Exception as e:
            response_data['available'] = 'no'
        return JsonResponse(response_data)
