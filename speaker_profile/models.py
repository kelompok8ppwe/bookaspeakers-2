from django.db import models
from registerPembicara.models import Pembicara

# Create your models here.
class Booking(models.Model):
    CITIES = [
        ('Jakarta', 'Jakarta'),
        ('Bandung', 'Bandung'),
        ('Bogor', 'Bogor'),
        ('Depok', 'Depok'),
    ]
    
    name = models.CharField(max_length=30)
    event_name = models.CharField(max_length=30)
    event_date = models.DateField()
    topic = models.CharField(max_length=20)
    city = models.CharField(max_length=7, choices=CITIES)
    phone_number = models.IntegerField()
    email = models.EmailField()
    speaker = models.ForeignKey(Pembicara, on_delete=models.CASCADE)

    def __str__(self):
        return self.name