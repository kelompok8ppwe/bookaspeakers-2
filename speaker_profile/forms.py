from django import forms
from .models import Booking
from django.forms import TextInput

class CreateBooking(forms.ModelForm):
    class Meta:
        model = Booking
        fields = ('name', 'event_name', 'event_date', 'topic', 'city', 'phone_number', 'email')
    def __init__(self, *args, **kwargs):
        super(CreateBooking, self).__init__(*args, **kwargs)
        self.fields['event_date'].widget = TextInput(attrs={
            'type': 'date',
            'class': 'dateinput',
        })
        self.fields['topic'].widget = TextInput(attrs={
            'data-url': "{% url speaker_profile:check_availability %}",
            'onkeyup': 'checkavailability(this)'
        })

