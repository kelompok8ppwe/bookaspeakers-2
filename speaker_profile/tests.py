from django.test import TestCase, Client
from .models import Booking
from .forms import CreateBooking
from registerPembicara.models import Pembicara

# Create your tests here.

class ProfileTest(TestCase):
    def createPembicara(self, nama_depan="namaku", nama_belakang="namaku", posisi="bos", eemail="abc@d.com", jenis="laki", kota='Jakarta', harga="200000", tanggal_lahir="1999-10-10", notelp='081234567890', keterangan="oke"):
        return Pembicara.objects.create(first_name=nama_depan, last_name=nama_belakang, position=posisi, email=eemail, gender=jenis, city=kota, fee=harga, birth_date=tanggal_lahir, phone=notelp, desc=keterangan)

    def createBooking(self, nama="namaku", nama_acara="compfest", tanggal_acara="2000-12-20", topik="web development", kota='Jakarta', notelp='081234567890', eemail="nethaniasonyavls@gmail.com"):
        c = self.createPembicara()
        thespeaker = Pembicara.objects.get(id=1)
        return Booking.objects.create(name=nama, event_name=nama_acara, event_date=tanggal_acara, topic=topik, city=kota, phone_number=notelp, email=eemail, speaker=thespeaker)

    def test_speaker_created(self):
        counter0 = Pembicara.objects.count()
        c = self.createPembicara()
        counter1 = Pembicara.objects.count()
        self.assertEqual(counter0 + 1, counter1)  
        self.assertTrue(isinstance(c, Pembicara))

    def test_booking_created(self):
        counter0 = Booking.objects.count()
        d = self.createBooking()
        counter1 = Booking.objects.count()
        self.assertEqual(counter0 + 1, counter1)  
        self.assertTrue(isinstance(d, Booking))
        self.assertEqual(d.__str__(), d.name)

    def test_fill_booking_form(self):
        p = self.createPembicara()
        c = Client()
        form_data = {
            'name': 'sonya',
            'event_name': 'sukuran',
            'event_date': '1999-1-1',
            'topic': 'apa',
            'city': 'Bandung',
            'phone_number': '081234567890',
            'email': 'abc@d.com',
        }
        form = CreateBooking(data = form_data)
        self.assertTrue(form.is_valid())
