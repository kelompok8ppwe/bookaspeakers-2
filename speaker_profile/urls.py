from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'speaker_profile'


urlpatterns = [
    path('<id>', views.profile, name='profile'),
    path('<id>/check_availability/', views.check_availability, name='check_availability'),
]