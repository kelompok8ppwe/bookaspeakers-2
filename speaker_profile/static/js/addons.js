$(document).ready(function() {
    $(".bookingform").on('submit', function(event) {
        alert("You have successfully booked this speaker. We will notify you about it shortly:)");
    })
    
    $( "input" ).click(function() {
        $("label").css("color", "#FFB84C");
        $(".formbutton").css("background-color", "#2C8C9C");
    });
    
    $("input").blur(function(){
        $("label").css("color", "#2C8C9C");
        $(".formbutton").css("background-color", "#FFB84C");
    });
  });